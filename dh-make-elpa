#!/usr/bin/perl

=head1 NAME

dh-make-elpa - helper for creating Debian packages from ELPA packages

=cut

use strict;
use warnings;

use DhMakeELPA;

exit DhMakeELPA->run;

__END__

=head1 SYNOPSIS

=over

=item dh-make-elpa [--pkg-emacsen] [make]

=item dh-make-elpa help

=back

=head1 DESCRIPTION

B<dh-make-elpa> will try to create the files required to build a
Debian source package from an unpacked GNU Emacs ELPA package.

B<dh-make-elpa> is an experimental script that performs a lot of
guesswork.  You should throughly verify the contents of the debian/
directory before uploading

=head2 TYPICAL USAGE

=over 4

# Set your name and email address (these will be used in F<debian/control>)
% export DEBFULLNAME="Jane Bloggs"
% export DEBEMAIL=j.s.bloggs@example.com

% git clone -o upstream https://foo.org/foo.git

% cd foo

% git reset --hard 1.0.0      # package latest stable release

% git branch --unset-upstream # detaches master branch from upstream remote

% dh-make-elpa --pkg-emacsen

% git add debian && git commit -m "initial Debianisation"

% git deborig

% dpkg-buildpackage -us -uc

=back

=head2 COMMANDS

=over

=item make

Default command if no command is given. Creates debianisation from
scratch.  Fails with an error if F<debian/> directory already
exists.

=item help

Displays short usage information.

=back

=head2 OPTIONS

=over

=item B<--pkg-emacsen>

Sets C<Maintainer>, C<Uploaders>, C<Vcs-Git> and C<Vcs-Browser> fields
in F<debian/control> according to the Team's conventions.

This option is useful when preparing a package for the Debian Emacs
Addon Packaging Team L<https://pkg-emacsen.alioth.debian.org>.

=item B<--version>

Specify the ELPA package version.  Useful when upstream failed to
include either a Package-Version: header or a -pkg.el file.

=back

=head2 TIPS

=over

=item Upstream remote

The remote for upstream's git repository should be called 'upstream'.
You can use:

=over 4

% git clone -o upstream https://foo/bar

=back

=back

=cut

=head1 AUTHOR

Originally written by Sean Whitton <spwhitton@spwhitton.name>,
currently maintained by Lev Lamberov <dogsleg@debian.org> and Debian
Emacsen team <debian-emacsen@lists.debian.org>.

A great deal of the library code used by B<dh-make-elpa>, and its
object-oriented structure, comes from dh-make-perl(1), written and
maintained by the Debian Perl Group.

=cut
